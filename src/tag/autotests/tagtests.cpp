/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2013  Vishesh Handa <me@vhanda.in>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "tagtests.h"

#include "tag.h"
#include "tagrelation.h"
#include "database.h"

#include "qtest_kde.h"
#include <KDebug>

#include <QDir>
#include <QTest>
#include <QSignalSpy>

#include <QSqlQuery>
#include <QSqlError>

TagTests::TagTests(QObject* parent)
    : QObject(parent)
    , m_db(0)
{
}

void TagTests::initTestCase()
{
    m_dbPath = m_tempDir.name() + QLatin1String("tagDB.sqlite");

    qRegisterMetaType<Item>();
    qRegisterMetaType<Tag>();
    qRegisterMetaType<Relation>();
    qRegisterMetaType<TagRelation>();
    qRegisterMetaType<KJob*>();
}

void TagTests::cleanupTestCase()
{
    QFile::remove(m_dbPath);
}

void TagTests::init()
{
    delete m_db;
    cleanupTestCase();

    m_db = new Database(this);
    m_db->setPath(m_dbPath);
    m_db->init();
}

void TagTests::insertTags(const QStringList& tags)
{
    foreach (const QString& tag, tags) {
        QSqlQuery insertQ;
        insertQ.prepare("INSERT INTO tags (name) VALUES (?)");
        insertQ.addBindValue(tag);

        QVERIFY(insertQ.exec());
    }
}

void TagTests::insertRelations(const QHash< int, QString >& relations)
{
    QHash< int, QString >::const_iterator iter = relations.constBegin();
    for (; iter != relations.constEnd(); iter++) {
        QSqlQuery insertQ;
        insertQ.prepare("INSERT INTO tagRelations (tid, rid) VALUES (?, ?)");
        insertQ.addBindValue(iter.key());
        insertQ.addBindValue(iter.value().toUtf8());

        QVERIFY(insertQ.exec());
    }
}


void TagTests::testTagFetchFromId()
{
    insertTags(QStringList() << "TagA" << "TagB" << "TagC");

    Tag tag = Tag::fromId("tag:2");
    QVERIFY(tag.name().isEmpty());
    QCOMPARE(tag.id(), QByteArray("tag:2"));

    TagFetchJob* job = tag.fetch();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(itemReceived(Item)));
    QSignalSpy spy2(job, SIGNAL(tagReceived(Tag)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    QCOMPARE(spy1.at(0).first().value<Item>().id(), tag.id());

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);

    Tag rTag = spy2.at(0).first().value<Tag>();
    QCOMPARE(rTag.id(), tag.id());
    QCOMPARE(rTag.name(), QLatin1String("TagB"));
}

void TagTests::testTagFetchFromName()
{
    insertTags(QStringList() << "TagA" << "TagB" << "TagC");

    Tag tag(QLatin1String("TagC"));
    QCOMPARE(tag.name(), QLatin1String("TagC"));
    QVERIFY(tag.id().isEmpty());

    TagFetchJob* job = tag.fetch();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(itemReceived(Item)));
    QSignalSpy spy2(job, SIGNAL(tagReceived(Tag)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    QCOMPARE(spy1.at(0).first().value<Item>().id(), QByteArray("tag:3"));

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);

    Tag rTag = spy2.at(0).first().value<Tag>();
    QCOMPARE(rTag.name(), tag.name());
    QCOMPARE(rTag.id(), QByteArray("tag:3"));
}

void TagTests::testTagFetchInvalid()
{
    Tag tag(QLatin1String("TagC"));
    TagFetchJob* job = tag.fetch();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(itemReceived(Item)));
    QSignalSpy spy2(job, SIGNAL(tagReceived(Tag)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(!job->exec());

    QCOMPARE(spy1.size(), 0);
    QCOMPARE(spy2.size(), 0);
    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);

    QCOMPARE(job->error(), (int)TagFetchJob::Error_TagDoesNotExist);
    QVERIFY(tag.id().isEmpty());
}

void TagTests::testTagCreate()
{
    Tag tag(QLatin1String("TagA"));
    TagCreateJob* job = tag.create();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(itemCreated(Item)));
    QSignalSpy spy2(job, SIGNAL(tagCreated(Tag)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    QCOMPARE(spy1.at(0).first().value<Item>().id(), QByteArray("tag:1"));

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);

    Tag rTag = spy2.at(0).first().value<Tag>();
    QCOMPARE(rTag.name(), tag.name());
    QCOMPARE(rTag.id(), QByteArray("tag:1"));

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), 0);
}

void TagTests::testTagCreate_duplicate()
{
    testTagCreate();

    Tag tag(QLatin1String("TagA"));
    TagCreateJob* job = tag.create();
    QVERIFY(job);

    QSignalSpy spy(job, SIGNAL(result(KJob*)));
    QVERIFY(!job->exec());

    QCOMPARE(spy.size(), 1);
    QCOMPARE(spy.at(0).size(), 1);
    QCOMPARE(spy.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), (int)TagCreateJob::Error_TagExists);

    QVERIFY(tag.id().isEmpty());
}

void TagTests::testTagModify()
{
    Tag tag(QLatin1String("TagA"));
    TagCreateJob* cjob = tag.create();

    QSignalSpy spy(cjob, SIGNAL(tagCreated(Tag)));
    cjob->exec();

    QCOMPARE(spy.size(), 1);
    QCOMPARE(spy.at(0).size(), 1);
    tag = spy.at(0).first().value<Tag>();

    QByteArray id = tag.id();
    tag.setName("TagB");

    ItemSaveJob* job = tag.save();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(itemSaved(Item)));
    QSignalSpy spy2(job, SIGNAL(tagSaved(Tag)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    QCOMPARE(spy1.at(0).first().value<Item>().id(), tag.id());

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);

    Tag rTag = spy2.at(0).first().value<Tag>();
    QCOMPARE(rTag.name(), QLatin1String("TagB"));
    QCOMPARE(rTag.id(), tag.id());

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), 0);
}

void TagTests::testTagModify_duplicate()
{
    insertTags(QStringList() << "TagB");

    Tag tag(QLatin1String("TagA"));
    TagCreateJob* cjob = tag.create();

    QSignalSpy spy(cjob, SIGNAL(tagCreated(Tag)));
    cjob->exec();

    QCOMPARE(spy.size(), 1);
    QCOMPARE(spy.at(0).size(), 1);
    tag = spy.at(0).first().value<Tag>();

    QByteArray id = tag.id();
    tag.setName("TagB");

    ItemSaveJob* job = tag.save();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(itemSaved(Item)));
    QSignalSpy spy2(job, SIGNAL(tagSaved(Tag)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(!job->exec());

    QCOMPARE(spy1.size(), 0);
    QCOMPARE(spy2.size(), 0);

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), (int)TagSaveJob::Error_TagExists);
}

void TagTests::testTagRemove()
{
    insertTags(QStringList() << "TagA" << "TagB" << "TagC");

    Tag tag = Tag::fromId(QByteArray("tag:1"));
    TagRemoveJob* job = tag.remove();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(itemRemoved(Item)));
    QSignalSpy spy2(job, SIGNAL(tagRemoved(Tag)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    QCOMPARE(spy1.at(0).first().value<Item>().id(), QByteArray("tag:1"));

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);

    Tag rTag = spy2.at(0).first().value<Tag>();
    QCOMPARE(rTag.id(), QByteArray("tag:1"));
    QVERIFY(rTag.name().isEmpty());

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), 0);

    QVERIFY(tag.name().isEmpty());

    QSqlQuery query;
    query.prepare("SELECT name from tags where id = ?");
    query.addBindValue(1);
    QVERIFY(query.exec());
    QVERIFY(!query.next());
}

void TagTests::testTagRemove_notExists()
{
    Tag tag = Tag::fromId(QByteArray("tag:1"));
    TagRemoveJob* job = tag.remove();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(itemRemoved(Item)));
    QSignalSpy spy2(job, SIGNAL(tagRemoved(Tag)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(!job->exec());

    QCOMPARE(spy1.size(), 0);
    QCOMPARE(spy2.size(), 0);
    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), (int)TagRemoveJob::Error_TagDoesNotExist);
}


void TagTests::testTagRelationFetchFromTag()
{
    insertTags(QStringList() << "TagA");

    QHash<int, QString> rel;
    rel.insert(1, "file:1");
    insertRelations(rel);

    Tag tag(QLatin1String("TagA"));

    TagRelation tagRel(tag);
    TagRelationFetchJob* job = tagRel.fetch();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(relationReceived(Relation)));
    QSignalSpy spy2(job, SIGNAL(tagRelationReceived(TagRelation)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    /*
     * FIXME!!
    QCOMPARE(spy1.at(0).first().value<Relation>().from().id(), QByteArray()
    QCOMPARE(spy1.at(0).first().value<Relation>().to().id(), &tagRel);
    */

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);
    TagRelation tagRel2 = spy2.at(0).first().value<TagRelation>();

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), 0);

    QCOMPARE(tagRel2.item().id(), QByteArray("file:1"));
    QCOMPARE(tagRel2.tag().name(), QLatin1String("TagA"));
    QCOMPARE(tagRel2.tag().id(), QByteArray("tag:1"));
}

void TagTests::testTagRelationFetchFromItem()
{
    insertTags(QStringList() << "TagA");

    QHash<int, QString> rel;
    rel.insert(1, "file:1");
    insertRelations(rel);

    Item item;
    item.setId("file:1");

    TagRelation tagRel(item);
    TagRelationFetchJob* job = tagRel.fetch();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(relationReceived(Relation)));
    QSignalSpy spy2(job, SIGNAL(tagRelationReceived(TagRelation)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    //QCOMPARE(spy1.at(0).first().value<Relation*>(), &tagRel);

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);
    TagRelation tagRel2 = spy2.at(0).first().value<TagRelation>();

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), 0);

    QCOMPARE(tagRel2.item().id(), QByteArray("file:1"));
    QCOMPARE(tagRel2.tag().id(), QByteArray("tag:1"));
    QVERIFY(tagRel2.tag().name().isEmpty());
}

void TagTests::testTagRelationSaveJob()
{
    insertTags(QStringList() << "TagA");

    Item item;
    item.setId("file:1");

    Tag tag = Tag::fromId(QByteArray("tag:1"));

    TagRelation rel(tag, item);
    TagRelationCreateJob* job = rel.create();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(relationCreated(Relation)));
    QSignalSpy spy2(job, SIGNAL(tagRelationCreated(TagRelation)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    //QCOMPARE(spy1.at(0).first().value<Relation*>(), &rel);

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);

    TagRelation tagRel2 = spy2.at(0).first().value<TagRelation>();
    QCOMPARE(tagRel2.item().id(), rel.item().id());
    QCOMPARE(tagRel2.tag().id(), rel.tag().id());
    QCOMPARE(tagRel2.tag().name(), rel.tag().name());

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), 0);

    QSqlQuery query;
    query.prepare("select rid from tagRelations where tid = 1");

    QVERIFY(query.exec());
    QVERIFY(query.next());
    QCOMPARE(query.value(0).toByteArray(), QByteArray("file:1"));
}

void TagTests::testTagRelationSaveJob_duplicate()
{
    insertTags(QStringList() << "TagA");

    QHash<int, QString> relHash;
    relHash.insert(1, "file:1");
    insertRelations(relHash);

    Item item;
    item.setId("file:1");

    Tag tag = Tag::fromId(QByteArray("tag:1"));

    TagRelation rel(tag, item);
    TagRelationCreateJob* job = rel.create();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(relationCreated(Relation)));
    QSignalSpy spy2(job, SIGNAL(tagRelationCreated(TagRelation)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(!job->exec());

    QCOMPARE(spy1.size(), 0);
    QCOMPARE(spy2.size(), 0);

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), (int)TagRelationCreateJob::Error_RelationExists);
}

void TagTests::testTagRelationRemoveJob()
{
    insertTags(QStringList() << "TagA");

    QHash<int, QString> relHash;
    relHash.insert(1, "file:1");
    insertRelations(relHash);

    Item item;
    item.setId("file:1");

    Tag tag = Tag::fromId(QByteArray("tag:1"));

    TagRelation rel(tag, item);
    TagRelationRemoveJob* job = rel.remove();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(relationRemoved(Relation)));
    QSignalSpy spy2(job, SIGNAL(tagRelationRemoved(TagRelation)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(job->exec());

    QCOMPARE(spy1.size(), 1);
    QCOMPARE(spy1.at(0).size(), 1);
    //QCOMPARE(spy1.at(0).first().value<Relation*>(), &rel);

    QCOMPARE(spy2.size(), 1);
    QCOMPARE(spy2.at(0).size(), 1);

    TagRelation tagRel2 = spy2.at(0).first().value<TagRelation>();
    QCOMPARE(tagRel2.item().id(), rel.item().id());
    QCOMPARE(tagRel2.tag().id(), rel.tag().id());
    QVERIFY(tagRel2.tag().name().isEmpty());

    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), 0);

    QSqlQuery query;
    query.prepare("select rid from tagRelations where tid = ?");
    query.addBindValue(1);
    QVERIFY(query.exec());
    QVERIFY(!query.next());
}

void TagTests::testTagRelationRemoveJob_notExists()
{
    Item item;
    item.setId("file:1");

    Tag tag = Tag::fromId(QByteArray("tag:1"));

    TagRelation rel(tag, item);
    TagRelationRemoveJob* job = rel.remove();
    QVERIFY(job);

    QSignalSpy spy1(job, SIGNAL(relationRemoved(Relation)));
    QSignalSpy spy2(job, SIGNAL(tagRelationRemoved(TagRelation)));
    QSignalSpy spy3(job, SIGNAL(result(KJob*)));
    QVERIFY(!job->exec());

    QCOMPARE(spy1.size(), 0);
    QCOMPARE(spy2.size(), 0);
    QCOMPARE(spy3.size(), 1);
    QCOMPARE(spy3.at(0).size(), 1);
    QCOMPARE(spy3.at(0).first().value<KJob*>(), job);
    QCOMPARE(job->error(), (int)TagRelationRemoveJob::Error_RelationDoesNotExist);
}

QTEST_KDEMAIN_CORE(TagTests)
